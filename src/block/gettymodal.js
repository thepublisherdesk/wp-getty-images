const { __ } = wp.i18n
const { Fragment, Component } = wp.element
const { apiFetch } = wp;
const { Button, TextControl,TextareaControl, SelectControl, Modal, Notice } = wp.components;
const { ENTER } = wp.keycodes;

import gettyLogoLetters from './icons/logo-letters'
import gettyLogoIcon from './icons/logo'
import SortGetty from './sortGetty'

export default class GettyModal extends Component {

	state = {
		s_query: '',
		the_results: [],
		the_results_page: 1,
		img_id_selected: '',
		img_id: 1172643920,
		img_thumbURL: "",
		img_title: "",
		img_alt: '',
		img_caption: "", //image.caption,
		img_description: '',
		img_sizes: [],
		img_size_value: "getty-default",
		img_credit: '', //image.artist,
		img_artist:'',
		the_image_uri:"", //image.display_sizes[0].uri,
		search_notice:'',
		downloading: false,
		isSearching:false,
		loadingMore:false,
		sortParams: '',
		imgs_per_row: 4,
		filtersOpen: false,
		shortLogo: false,
	}

	searchWithSortParams = (params) => {

		this.cancelDownload(); // clear selected image state vars
		this.setState( { the_results: [], sortParams: params, isSearching: true }, () => {
			apiFetch({
				path: `/gettyimages/v1/search?s=` + this.state.s_query + `&page=` + this.state.the_results_page + this.state.sortParams,
				method: "GET",
			})
			.then( results => {
				let new_results = []
				if(results.error == true){
					this.setState( { the_results: results.results } )
					this.setState( { search_notice: results.message } )
					return;
				}
				this.setState( { isSearching: false } )
				this.setState( { the_results: results } )
				this.setState( { the_results_page: (this.state.the_results_page + 1) } ) // set next page number
			});
		} )

	}

	openCloseFilters = (params) => {
		this.setState( { filtersOpen: params } )
	}

	setImageDetailSize = (selected) => {
		let imageSizes = [];
		for (const [size, count] of Object.entries(wpSettings.imageSizes)) {
			let option = {
				label:`${size.charAt(0).toUpperCase() + size.slice(1)} - ${count.width}x${count.height}`,
				value:size
			}
			imageSizes.push(option);

		}
		this.setState( { img_sizes: imageSizes } )

	}

	getGettyImages = () => {

		this.cancelDownload(); // clear selected image state vars
		this.props.setAttributes( { isModalOpen: true } )
		this.setState( { the_results: [], isSearching:true } )
		apiFetch( {
			path: `/gettyimages/v1/search?s=` + this.state.s_query + `&page=1` + this.state.sortParams,
			method: "GET",
		})
		.then( results => {
			let new_results = []
			if(results.error == true){
				this.setState( { the_results: results.results, isSearching:false } )
				this.setState( { search_notice: results.message } )
				return;
			}
			this.setState( { the_results: results, isSearching:false } )
			this.setState( { the_results_page: (this.state.the_results_page + 1) } ) // set next page number
		});

	}

	getMoreGettyImages = () => {

		this.cancelDownload(); // clear selected image state vars
		this.state.loadingMore = true;

		apiFetch( {
			path: `/gettyimages/v1/search?s=` + this.state.s_query + `&page=` + this.state.the_results_page + this.state.sortParams,
			method: "GET",
		})
		.then( results => {
			let new_results = []
			this.state.loadingMore = false;
			if(results.error == true){
				this.setState( { the_results: results.results } )
				this.setState( { search_notice: results.message } )
				return;
			}
			new_results = this.state.the_results.concat(results) // append results to existing list
			this.setState( { the_results: new_results } )
			this.setState( { the_results_page: (this.state.the_results_page + 1) } ) // set next page number
		});

	}

	showImageInfo = (img) => {
		this.setState( { img_id_selected: img.id } )
		this.setState( { img_id: img.id } )
		this.setState( { img_title: img.title } )
		this.setState( { img_alt: img.title } )
		this.setState( { img_caption: img.caption } )
		this.setState( { img_thumbURL: img.display_sizes[0].uri } )

		apiFetch( {
			path: `/gettyimages/v1/images?imageID=` + img.id,
			method: "GET",
		})
		.then( results => {
			let imageSizes = [];

			if (results.length == 0 ) {
				return;
			}

			if ( results[0].download_sizes !== undefined ) {
				results[0].download_sizes.forEach(size => {
					if(size.name == "medium"){
						imageSizes.push(size.height);
					}
				});
				this.setState( {
					img_credit: results[0].credit_line,
					img_artist: results[0].artist,
					img_sizes: imageSizes
				} )
			}
		});
	}

	cancelDownload = () => {
		this.setState( { img_id_selected: '' } )
		this.setState( { img_id: '' } )
		this.setState( { img_title: '' } )
		this.setState( { img_alt: '' } )
		this.setState( { img_caption: '' } )
		this.setState( { img_thumbURL: '' } )
		this.setState( { img_description: '' } )
		this.setState( { img_sizes: [] } )
		this.setState( { img_credit: '' } )
	}

	downloadImage = (id) => {
		this.setState( { downloading: true } )
		this.setState( { the_results: [] } )
		let postID = wp.data.select("core/editor").getCurrentPostId();
		apiFetch( {
			path: `/gettyimages/v1/download?img_id=${this.state.img_id}&post_id=${postID}&img_title=${this.state.img_title}&img_caption=${this.state.img_caption}&img_alt=${this.state.img_alt}&img_sizes=${this.state.img_sizes}&img_credit=${this.state.img_credit}&img_artist=${this.state.img_artist}`,
			method: "GET"
		})
		.then( results => {
			this.setState( { downloading: false } )
			this.props.setAttributes( { isModalOpen: false } )
			if ( this.props.attributes.download_into == "featured-image" ) {
				this.props.setAttributes( {
					isImageSelected: true,
				} )
				wp.data.dispatch('core/editor').editPost({featured_media:results.ID})
				wp.data.dispatch('core/notices').createNotice(
					'success', // Can be one of: success, info, warning, error.
					'Getty Image has been downloaded and set as Featured Image.', // Text string to display.
					{
						isDismissible: true, // Whether the user can dismiss the notice.
					}
				);
			} else {
				this.props.setAttributes( {
					isImageSelected: true,
					url: results.guid,
					imageID: results.ID,
					alt: this.state.img_alt,
					caption: results.post_excerpt,
				} )
			}
		});
	}

	updateImagesPerRow = () => {
		if(window.innerWidth < 640 && this.state.imgs_per_row != 2 ) {
			this.setState( { imgs_per_row: 2 } ) 
			this.setState( { shortLogo: true } ) 
		}else if(window.innerWidth < 1024 && this.state.imgs_per_row != 3 ) {
			this.setState( { imgs_per_row: 3 } ) 
			this.setState( { shortLogo: false } ) 
		}else if(window.innerWidth > 1023 && this.state.imgs_per_row != 4 ) {
			this.setState( { imgs_per_row: 4 } ) 
			this.setState( { shortLogo: false } ) 
		}
	}
	closeModal = (isFeatureImageModal) => {
		this.props.setAttributes( { isModalOpen: false } );		
		if ( isFeatureImageModal ) {
			let currentBlockID = wp.data.select( 'core/block-editor' ).getSelectedBlockClientId()
			wp.data.dispatch( 'core/block-editor' ).removeBlock( currentBlockID )
		}
	}
	componentDidMount() {
    this.updateImagesPerRow();
    window.addEventListener("resize", this.updateImagesPerRow);
  }

	componentWillUnmount() {
    window.removeEventListener("resize", this.updateImagesPerRow);
  }

	render() {

		const { attributes: { imageID, isImageSelected, url, download_into, isModalOpen, isFeatureImageModal }, setAttributes } = this.props
		const { s_query, the_results, the_results_page, img_id_selected, img_id, img_thumbURL, img_title, img_alt, img_caption, img_description, img_sizes, img_size_value, img_credit, img_artist, search_notice, downloading, isSearching, loadingMore, imgs_per_row, filtersOpen, shortLogo } = this.state

		// https://stackoverflow.com/questions/8495687/split-array-into-chunks
		const array_chunks = (array, chunk_size) => Array(Math.ceil(array.length / chunk_size)).fill().map((_, index) => index * chunk_size).map(begin => array.slice(begin, begin + chunk_size));
		const the_results_chunks = array_chunks( the_results, imgs_per_row ) //how many images per row
		let activeSidebar = ( img_id_selected > 0 ) ? 'active' : '' ;

		return (

			<Fragment>
				<div className={ this.props.className }>

					<div className="getty-search-container">
						<TextControl
							placeholder = "Search Image"
							value = { s_query || '' }
							onChange = { ( value ) => this.setState( { s_query: value } ) }
							onKeyDown = { ( event ) => {
								const { keyCode } = event;
								if ( keyCode === ENTER ) {
									this.getGettyImages()
								}
							} }
						/>
						<Button onClick={ this.getGettyImages }>&#128269;</Button>
					</div>

					<div>
						{ isModalOpen && (
							<Modal
								title=""
								className="getty-images-modal"
							>

							<div className="g-header">
								<div className="logo">
									{ ( shortLogo ? gettyLogoIcon : gettyLogoLetters ) }
								</div>
								<div className="getty-search-container">
									<TextControl
										className="getty-search-input"
										placeholder = "Search Image"
										value = { s_query || '' }
										onChange = { ( value ) => this.setState( { s_query: value } ) }
										onKeyDown = { ( event ) => {
											const { keyCode } = event;
											if ( keyCode === ENTER ) {
												this.getGettyImages()
											}
										} }
									/>
									<Button isSecondary onClick={ () => {
										this.getGettyImages()
									} }>&#128269;</Button>
									<Button className="getty-modal-close" isSecondary onClick={ () => {
										this.closeModal(isFeatureImageModal)
									} }>Close</Button>
								</div>
							</div>								

							{ (the_results.length == 0 && isSearching == false && downloading == false) && (
								<Notice status="default" className="modal-notice" isDismissible={false}>
									<p>Enter a phrase in the input box to search for images.</p>
								</Notice>
							) }

							<SortGetty callbackFromGettyModalFilterState={this.openCloseFilters} callbackFromGettyModal={this.searchWithSortParams} />

								<div className={"getty-images-content " + ( filtersOpen ? 'open' : '' ) }>

									<div className="getty-images-container">
										{ isSearching == true && (
											<div className="letter-loadind">
												<div className="l-1 letter">S</div>
												<div className="l-2 letter">e</div>
												<div className="l-3 letter">r</div>
												<div className="l-4 letter">c</div>
												<div className="l-5 letter">h</div>
												<div className="l-6 letter">i</div>
												<div className="l-7 letter">n</div>
												<div className="l-8 letter">g</div>
												<div className="l-9 letter">.</div>
												<div className="l-10 letter">.</div>
												<div className="l-11 letter">.</div>
											</div>
										) }
										<div className="getty-images-archive">

											{
												// images fill container width and make them equal height
												// https://codepen.io/blimpage/pen/obWdgp
											}
											{ the_results_chunks.map((row, i) => {
												return (
													<div key={'row-'+i} style={{display:'flex'}}>
													{ row.map((image, index) => {
														let aspect_ratio = ( image.download_sizes.length > 0 ) ? image.download_sizes[0].width / image.download_sizes[0].height : '1.5' ;
														return (
															<div key={image.id} style={{flex:aspect_ratio}} className={ (img_id_selected == image.id) ? 'getty-image-container' : 'getty-image-container' }>
																<img title={image.title} alt={image.title} src={image.display_sizes[0].uri } onClick={ () => this.showImageInfo(image) }/>
															</div>
														)
													})}
												</div>
												)
											})}

										</div>
										{ the_results.length != 0 && (
											<Button className={"getty-search-btn-more " + ( loadingMore ? 'loading' : '' ) } onClick={ this.getMoreGettyImages }>load more</Button>
										) }
									</div>
									<div className={ activeSidebar + " getty-images-sidebar" }>
										{ !downloading && img_title && (
											<div className="getty-image-details">
												<div className="getty-image-details_header">

													<img src={ img_thumbURL } alt={ img_alt }/>
													<h3>{ img_title }</h3>
													<TextControl
														label="Title"
														value={ img_title || '' }
														onChange={ ( title ) => this.setState( { img_title: title.toLowerCase() } ) }
													/>
													<TextControl
														label="Alt Text"
														value={ img_alt || '' }
														onChange={ ( title ) => this.setState( { img_alt: title } ) }
													/>
													<TextareaControl
														label="Caption"
														value={ img_caption || '' }
														onChange={ ( caption ) => this.setState( { img_caption: caption } ) }
													/>
													{isFeatureImageModal == false &&
														(<SelectControl
															label={ __( 'Download Options:' ) }
															value={ download_into } // e.g: value = [ 'a', 'c' ]
															onChange={ ( option ) => { setAttributes( { download_into:option } ) } }
															options={ [
																{ value: 'in-post', label: 'Download into post' },
																{ value: 'featured-image', label: 'Download and set as featured' },
															] }
														/>)
													}

													<Button isSecondary onClick={ () => this.downloadImage(img_id) } >Download</Button>
													<Button isSecondary onClick={ () => this.cancelDownload() } >Cancel</Button>
													<p className="getty-image-detail_id">Image #: { img_id }</p>
													<p className="getty-image-detail_credit">Credit: { img_credit }</p>

												</div>
											</div>
										)}
									</div>
								</div>

								{ downloading && (
								<div className="downloading-loader">
									<div className="letter-loadind">
										<div className="l-1 letter">D</div>
										<div className="l-2 letter">o</div>
										<div className="l-3 letter">w</div>
										<div className="l-4 letter">n</div>
										<div className="l-5 letter">l</div>
										<div className="l-6 letter">o</div>
										<div className="l-7 letter">a</div>
										<div className="l-8 letter">d</div>
										<div className="l-9 letter">i</div>
										<div className="l-10 letter">n</div>
										<div className="l-11 letter">g</div>
										<div className="l-12 letter">.</div>
										<div className="l-13 letter">.</div>
										<div className="l-14 letter">.</div>
									</div>
								</div>
								) }

							</Modal>
						) }
					</div>

				</div>
			</Fragment>
		)
	}
}

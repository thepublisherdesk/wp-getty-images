const { Button } = wp.components;
const { Fragment } = wp.element;
/**
 * * Expand "Featured Image" Panel
 * https://github.com/WordPress/gutenberg/blob/a7f4269d370750f5ae578803e126dd4be84fd3cf/editor/components/post-featured-image/README.md
 */
function wrapPostFeaturedImage( OriginalComponent ) { 
	return function( props ) {
		return (
			<Fragment>
				<OriginalComponent { ...props } />
				<Button className="components-button editor-post-featured-image__toggle" onClick={ () => { 
					// use data dispatch to open getty search modal and set that download is actually a set featured image
					// https://developer.wordpress.org/block-editor/packages/packages-data/
					let gettyModal = wp.blocks.createBlock( 'cgb/block-wp-getty-images', {
						isModalOpen:true,
						isFeatureImageModal:true,
						download_into:"featured-image"
					} );
					wp.data.dispatch( 'core/editor' ).insertBlock( gettyModal )
				} }>Set featured image (Getty)</Button>
      </Fragment>
    );
	}
}

wp.hooks.addFilter( 
	'editor.PostFeaturedImage', 
	'block-wp-getty-images/wrap-post-featured-image', 
	wrapPostFeaturedImage
);

const { __ } = wp.i18n
const { Component } = wp.element
const { Button, RadioControl, SelectControl, CheckboxControl } = wp.components

import iconFilter from './icons/filter'

export default class SortGetty extends Component {

  state = {
    orientations: '',
    sort_order: 'best_match',
    compositions: '',
    editorial_segments: '',
    params: '',
    openFilters: false,
  }

  cancel = (event) => {
    // console.log("Cancel");
  }
  
  addFilter = () => {
    let concatenatedParams = ''
    if ( this.state.orientations ) {
      concatenatedParams += '&orientations=' + this.state.orientations
    }
    if ( this.state.sort_order ) {
      concatenatedParams += '&sort_order=' + this.state.sort_order
    }
    if ( this.state.editorial_segments ) {
      concatenatedParams += '&editorial_segments=' + this.state.editorial_segments
    }
    if ( this.state.compositions ) {
      concatenatedParams += '&compositions=' + this.state.compositions
    }
    this.setState( { params: concatenatedParams }, () => { this.props.callbackFromGettyModal( this.state.params ) } )
  }

  openFilterOptions = (event) => {
    if ( this.state.openFilters == false ) {
      this.setState( { openFilters: true } )
      this.props.callbackFromGettyModalFilterState( true )
    } else {
      this.setState( { openFilters: false } )
      this.props.callbackFromGettyModalFilterState( false )
    }
  }

  render() {

    const { orientations, sort_order, openFilters, compositions } = this.state

    return (
        <div className="filters-menu">

          <div className="sort-bar">
            <Button className={ ( openFilters ? 'open' : '' ) } onClick={ this.openFilterOptions }>{ iconFilter } FILTERS</Button>
            <ul>
              <li>All</li>
              <li><Button onClick={ () => { this.setState( { editorial_segments: 'sport' }, () => { this.addFilter() } ) } } className={(this.state.editorial_segments == 'sport' ? 'active' : '')}>Sports</Button></li>
              <li><Button onClick={ () => { this.setState( { editorial_segments: 'entertainment' }, () => { this.addFilter() } ) } } className={(this.state.editorial_segments == 'entertainment' ? 'active' : '')}>Entertainment</Button></li>
              <li><Button onClick={ () => { this.setState( { editorial_segments: 'news' }, () => { this.addFilter() } ) } } className={(this.state.editorial_segments == 'news' ? 'active' : '')}>News</Button></li>
              <li><Button onClick={ () => { this.setState( { editorial_segments: 'archival' }, () => { this.addFilter() } ) } } className={(this.state.editorial_segments == 'archival' ? 'active' : '')}>Archival</Button></li>
            </ul>
          </div>

          <div className={'components-modal__filters ' + ( openFilters ? 'open' : '' ) }>
            <SelectControl
              label={ __( 'CATEGORIES' ) }
              className={'cats-segments'}
              value={ this.state.editorial_segments }
              onChange={ ( editorial_segments ) => { this.setState( { editorial_segments }, () => { this.addFilter() } ) } }
              options={ [
                  // { value: null, label: 'Select a Segment', disabled: true },
                  { value: '', label: 'All' },
                  { value: 'sport', label: 'Sports' },
                  { value: 'entertainment', label: 'Entertainment' },
                  { value: 'news', label: 'News' },
                  { value: 'archival', label: 'Archival' },
              ] }
            />
            <RadioControl
              label="SORT BY"
              selected={ sort_order }
              options={ [
                  { label: 'Best Match', value: 'best_match' },
                  { label: 'Newest', value: 'newest' },
                  { label: 'Oldest', value: 'oldest' },
                  { label: 'Most Popular', value: 'most_popular' },
                  { label: 'Random', value: 'random' },
              ] }
              onChange={ ( value ) => { 
                this.setState( { sort_order: value }, () => { this.addFilter() } ) 
              } }
            />
            <RadioControl
              label="ORIENTATION"
              selected={ orientations }
              options={ [
                  { label: 'Horizontal', value: 'horizontal' },
                  // { label: 'Vertical', value: 'vertical' },
                  { label: 'Square', value: 'square' },
                  { label: 'Panoramic Horizontal', value: 'panoramic_horizontal' },
                  // { label: 'Panoramic Vertical', value: 'panoramic_vertical' },
              ] }
              onChange={ ( value ) => { 
                this.setState( { orientations: value }, () => { this.addFilter() } )
              } }
            />
            <SelectControl
              label={ __( 'COMPOSITION' ) }
              className={'comps-segments'}
              value={ this.state.compositions }
              onChange={ ( compositions ) => { this.setState( { compositions }, () => { this.addFilter() } ) } }
              options={ [
                  { value: null, label: 'Select a Composition', disabled: true },
                  { value: 'abstract', label: 'Abstract' }, 
                  { value: 'candid', label: 'Candid' }, 
                  { value: 'close_up', label: 'Close Up' }, 
                  { value: 'copy_space', label: 'Copy Space' }, 
                  { value: 'cut_out', label: 'Cut Out' }, 
                  { value: 'full_frame', label: 'Full Frame' }, 
                  { value: 'full_length', label: 'Full Length' }, 
                  { value: 'headshot', label: 'Headshot' }, 
                  { value: 'looking_at_camera', label: 'Looking at Camera' }, 
                  { value: 'macro', label: 'Macro' }, 
                  { value: 'portrait', label: 'Portrait' }, 
                  { value: 'sparse', label: 'Sparse' }, 
                  { value: 'still_life', label: 'Still Life' }, 
                  { value: 'three_quarter_length', label: 'Three Quarter Length' }, 
                  { value: 'waist_up', label: 'Waist Up' },

              ] }
            />
          </div>          

        </div>
    )

  }
}
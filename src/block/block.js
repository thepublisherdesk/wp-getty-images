/**
 * BLOCK: wp-getty-images
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './editor.scss';
import './style.scss';

// Icons
import gettyLogo from './icons/logo'
import gettyLogoLetters from './icons/logo-letters'
import FeaturedImageGettyButton from './featuredImageGetty'  // loads button on main document sidebar panel
import GettyModal from './gettymodal'

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const { Fragment } = wp.element;
const { apiFetch } = wp;
const { InspectorControls } = wp.blockEditor
const { createHigherOrderComponent, withState } = wp.compose;
const { PanelBody, TextControl } = wp.components

registerBlockType( 'cgb/block-wp-getty-images', {
	title: __( 'Getty Image' ),
	icon: gettyLogo,
	category: 'common',
	keywords: [
		__( 'Getty' ),
		__( 'Images' ),
		__( 'gallery' ),
	],
	transforms: {
		from: [
			{
				type: 'block',
				blocks: [ 'core/image' ], // wildcard - match any block
				transform: function( attributes ) {
					return wp.blocks.createBlock( 'cgb/block-wp-getty-images', {
						url: attributes.url,
						alt: attributes.alt,
						caption: attributes.caption,
						id: attributes.id,
						imageID: attributes.id,
						link: attributes.link,
						linkDestination: attributes.linkDestination,
						isImageSelected:false
					} );
				},
			},
		],
		to: [
			{
				type: 'block',
				blocks: [ 'core/image' ],
				transform: function( attributes ) {
					return wp.blocks.createBlock( 'core/image', {
						id: attributes.imageID,
						url: attributes.url,
						alt: attributes.alt,
						caption: attributes.caption,
						link: attributes.link,
						linkDestination: attributes.linkDestination,
					} );
				},
			},
		],
	},
	attributes: {
		isImageSelected: {
			type: 'boolean',
			default:false
		},
		imageID: {
			type: 'integer',
			default:0
		},
		isModalOpen: {
			type: 'boolean',
			default:false
		},
		isFeatureImageModal:{
			type: 'boolean',
			default:false
		},
		url: {
			type: 'string',
		},
		alt:{
			type: 'string',
		},
		link:{
			type: 'string',
		},
		title: {
			type: 'string',
		},
		caption: {
			type: 'string',
		},
		orientation: {
			type: 'string',
		},
		download_sizes: {
			type: 'array',
		},
		download_into:{
			type: 'string',
		},
	},

	edit: ( props ) => {

		const { isSelected, attributes, setAttributes } = props
		const { imageID, url, alt, isImageSelected, title, caption, orientation, download_sizes, download_into } = attributes
		const gettyBlock = () => {

			if ( !isImageSelected ) {
				return(
					<div className="gettyBlock">
						<div className="logo">
							{ gettyLogoLetters }
						</div>
						<GettyModal { ...{ attributes, setAttributes } } />
					</div>
				)
			} else {

				let postBlocks = wp.data.select('core/block-editor').getBlocks();
				let selectedBlockClientId = wp.data.select('core/block-editor').getSelectedBlockClientId();

				postBlocks.forEach((block, i) => {
					if(block.clientId == selectedBlockClientId){

						if(download_into == "featured-image"){
							wp.data.dispatch( 'core/editor' ).removeBlock( selectedBlockClientId )
						}else{
							let newImage = wp.blocks.createBlock( 'core/image', {
								id: imageID,
								url: url,
								alt: alt,
								caption: caption
							} )
							wp.data.dispatch( 'core/block-editor' ).replaceBlock( selectedBlockClientId, newImage )
						}
						
						setAttributes( { isImageSelected: false, download_into: "" } )
					}

				});

			}
		}
		return (
			<div className={ props.className }>
				<Fragment>

					{ gettyBlock() }

				</Fragment>
			</div>
		);
	},
	save: ( props ) => {
		return (
			<div className={ props.className }>
				<p>The Getty Image will be here</p>
			</div>
		);
	},
} );


const withInspectorControls =  createHigherOrderComponent( ( BlockEdit ) => {
    return ( props ) => {

		if ( props.name !== "core/image" || typeof props.attributes.id == 'undefined' ) {
			return (
				<BlockEdit { ...props } />
			);
		}
		let isSavingPost = wp.data.select('core/editor').isSavingPost();
		let imageCredit = props.attributes.credit ? props.attributes.credit : '';
		
		if ( imageCredit == '' ) {
			apiFetch( {
				path: `/gettyimages/v1/media?id=` + props.attributes.id,
				method: "GET",
			})
			.then( results => {
				props.setAttributes( { credit:results } )
			});
		}

		if ( props.attributes.caption == '' ) {
			apiFetch( {
				path: `/gettyimages/v1/media?id=${props.attributes.id}&check=caption` ,
				method: "GET",
			})
			.then( results => {
				props.setAttributes( { caption:results } )
			});
		}

		if ( isSavingPost && typeof props.attributes.credit !== "undefined" ) {
			apiFetch( {
				path: `/gettyimages/v1/media?id=${props.attributes.id}&credit_line=${props.attributes.credit}`,
				method: "GET",
			})
			.then( results => {
				let creditLine = `Photo by ${results.img_artist} / ${results.img_credit}`
				props.setAttributes( { credit:results } )
			});
		}
		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<PanelBody title={ __( 'Media Credit', 'advanced-gutenberg-blocks' ) }>
						<TextControl
							label="Enter the creditor name"
							value={ imageCredit }
							onChange={ ( value ) => props.setAttributes( { credit:value } ) }
						/>
					</PanelBody>
				</InspectorControls>
			</Fragment>
		);
	};
}, "withInspectorControl" );
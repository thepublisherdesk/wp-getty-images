const iconFilter = <svg width="26px" height="18px" xmlns="http://www.w3.org/2000/svg">
<g>
	<polygon points="26,4.9 0,4.9 0,3 26,2.9 "></polygon>
	<polygon points="26,14.9 0,14.9 0,13 26,12.9 "></polygon>
	<circle cx="8.1" cy="3.9" r="3.5"></circle>
	<circle cx="17.5" cy="13.9" r="3.5"></circle>
</g>
</svg>;

export default iconFilter;
<?php

/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) {
	exit;
}
define('PD_GETTY_IMAGES_PATH', dirname(__FILE__));
define('PD_GETTY_IMAGES_URI', plugin_dir_url(__DIR__));

require_once 'getty-endpoints.php';
require_once 'setting.php';

// define the post_thumbnail_html callback 
add_filter( 'post_thumbnail_html', 'pd_filter_post_thumbnail_html', 10, 5 ); 
function pd_filter_post_thumbnail_html( $html, $post_id, $post_thumbnail_id, $size, $attr ) { 
	
	global $wp_query;
	if(!is_single()) return $html;
	if($wp_query->queried_object_id !== $post_id) return $html;

	$image = $html;
	$getty_details = get_getty_details($post_thumbnail_id);

	if($getty_details && count($getty_details) > 0){
		$image = "";
		$img_credit = $getty_details['img_credit'];
		$image_details = ($img_credit) ? '<figcaption>(' . $img_credit . ')</figcaption>' : '';
		$the_post_thumbnail_caption = get_the_post_thumbnail_caption( $post_id );

		// If caption is not empty check if img_credit and img_artist are in string
		if(!empty($the_post_thumbnail_caption)){
			add_filter( 'the_post_thumbnail_caption','tpd_set_post_thumbnail_caption__false', 10, 1 );
			if( is_string($img_credit) && $img_credit !== '' && $img_credit){
				if( strpos($the_post_thumbnail_caption, $img_credit) !== false ){
					$image_details = "<figcaption>$the_post_thumbnail_caption</figcaption>";
				}else{
					$image_details = "<figcaption>$the_post_thumbnail_caption ($img_credit)</figcaption>";
				}
			}else{
				$image_details = "<figcaption>$the_post_thumbnail_caption</figcaption>";
			}
		}

		$image = '<figure class="wp-block-image">';
		$image .= $html;
		$image .= $image_details;
		$image .= '</figure>';
	}

	return $image;
}
function tpd_set_post_thumbnail_caption__false( $caption )
{
	return false;
}
add_filter('the_content', 'pd_getty_show_caption');
function pd_getty_show_caption( $content ){

	if( !is_single() ) 
		return $content;
	
	$image_credited = false;
	if(has_blocks( $content )){
		$blocks = parse_blocks( $content );
		$content = '';
		foreach ($blocks as $key => $block) {
			if($block['blockName'] == 'core/image'){

				if(!isset($block['attrs']['id'])){
					continue;
				}

				$getty_details = get_getty_details($block['attrs']['id']);
				$img_credit = $getty_details['img_credit'];
				$image_credited = false;
				$has_credit = false;
				preg_match('/<figcaption>(.*)<\/figcaption>/',$block['innerHTML'],$matches);

				if(isset($matches[1])){		
					if( strpos($matches[1], $img_credit) !== false ){
						$has_credit = true;
					}
					if(!$has_credit){
						$image_details = "\t($img_credit)</figcaption></figure>";
						$block['innerContent'][0] = str_replace('</figcaption></figure>', $image_details, $block['innerContent'][0]);
					}
				}else{
					$image_credited = true;
					$img_credit = ($img_credit) ? '(' . $img_credit . ')' : '(via Getty Images)';
					$image_details = "\t<figcaption>$img_credit</figcaption></figure>";
					$block['innerContent'][0] = str_replace('</figure>', $image_details, $block['innerContent'][0]);
				}

			}
			
			$content .= render_block( $block );
		}
	}

	return $content;

}
function get_getty_details($attachment_id)
{
	if(!$attachment_id) return;

	$getty_details = get_post_meta( $attachment_id, 'getty_details', true);

	if(!$getty_details) return;

	return $getty_details;
}
/**
 * Get size information for all currently-registered image sizes.
 *
 * @global $_wp_additional_image_sizes
 * @uses   get_intermediate_image_sizes()
 * @return array $sizes Data for all currently-registered image sizes.
 */
function get_image_sizes() {
	global $_wp_additional_image_sizes;

	$sizes = array();
	$wp_image_sizes = get_intermediate_image_sizes();
	// error_log(print_r($wp_image_sizes,true));
	foreach ( $wp_image_sizes as $_size ) {
		if ( in_array( $_size, $wp_image_sizes ) ) {
			$sizes[ $_size ]['name']  = $_size;
			$sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
			$sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
			$sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
		} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
			$sizes[ $_size ] = array(
				'name'  => $_size,
				'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
				'height' => $_wp_additional_image_sizes[ $_size ]['height'],
				'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
			);
		}
	}

	return $sizes;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * Assets enqueued:
 * 1. blocks.style.build.css - Frontend + Backend.
 * 2. blocks.build.js - Backend.
 * 3. blocks.editor.build.css - Backend.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function wp_getty_images_cgb_block_assets()
{

	// Register block editor script for backend.
	wp_register_script(
		'wp_getty_images-cgb-block-js', // Handle.
		plugins_url('/dist/blocks.build.js', dirname(__FILE__)), // Block.build.js: We register the block here. Built with Webpack.
		array('wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'jquery'), // Dependencies, defined above.
		null, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
		true // Enqueue the script in the footer.
	);

	// Register block editor styles for backend.
	wp_register_style(
		'wp_getty_images-cgb-block-editor-css', // Handle.
		plugins_url('dist/blocks.editor.build.css', dirname(__FILE__)), // Block editor CSS.
		array('wp-edit-blocks'), // Dependency to include the CSS after it.
		null // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
	);

	// WP Localized globals. Use dynamic PHP stuff in JavaScript via `cgbGlobal` object.
	wp_localize_script(
		'wp_getty_images-cgb-block-js',
		'cgbGlobal', // Array containing dynamic data for a JS Global.
		[
			'pluginDirPath' => plugin_dir_path(__DIR__),
			'pluginDirUrl'  => plugin_dir_url(__DIR__),
			// Add more data here that you want to access from `cgbGlobal` object.
		]
	);
	wp_localize_script(
		'wp_getty_images-cgb-block-js',
		'wpAjax', // Array containing dynamic data for a JS Global.
		[
			'url' => admin_url('admin-ajax.php'),
			'pluginUrl' => PD_GETTY_IMAGES_URI,
			'siteUrl' => get_site_url()
			// Add more data here that you want to access from `cgbGlobal` object.
		]
	);
	wp_localize_script(
		'wp_getty_images-cgb-block-js',
		'wpSettings', // Array containing dynamic data for a JS Global.
		[
			'imageSizes' => get_image_sizes(),
			// Add more data here that you want to access from `cgbGlobal` object.
		]
	);
	/**
	 * Register Gutenberg block on server-side.
	 *
	 * Register the block on server-side to ensure that the block
	 * scripts and styles for both frontend and backend are
	 * enqueued when the editor loads.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
	 * @since 1.16.0
	 */
	register_block_type(
		'cgb/block-wp-getty-images',
		array(
			// Enqueue blocks.build.js in the editor only.
			'editor_script' => 'wp_getty_images-cgb-block-js',
			// Enqueue blocks.editor.build.css in the editor only.
			'editor_style'  => 'wp_getty_images-cgb-block-editor-css',
		)
	);
}

add_action('init', 'wp_getty_images_cgb_block_assets'); // Hook: Block assets.

add_action( 'admin_enqueue_scripts', 'wp_getty_images_enqueue_scripts' );
function wp_getty_images_enqueue_scripts()
{
	wp_localize_script(
		'wp_getty_images_script',
		'wpAjax', // Array containing dynamic data for a JS Global.
		[
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('save_getty_token_nonce'),
			// Add more data here that you want to access from `cgbGlobal` object.
		]
	);

}

function tpd_get_images_credit_line()
{
	global $post;

	$images = array();
	if ( $post && has_blocks($post->post_content)) {
		$blocks = parse_blocks( $post->post_content );
		foreach ($blocks as $key => $block) {
			if($block['blockName'] == 'core/image'){

				if(!isset($block['attrs']['id'])){
					continue;
				}

				$images[$block['attrs']['id']]['ID'] = $block['attrs']['id'];
				$images[$block['attrs']['id']]['getty_details'] = get_post_meta($block['attrs']['id'], 'getty_details', true);

			}
		}
	}

	return $images;
}
 
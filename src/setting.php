<?php
/**
 * Register a admin menu page.
 */
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
function wpdocs_register_my_custom_menu_page() {
    add_submenu_page( 
        'options-general.php',
        'WP Getty Images',
        'WP Getty Images',
        'manage_options',
        'wp-getty-images',
        'render_wp_getty_images_index'
    );
}

function render_wp_getty_images_index()
{
    echo "<h1>WP Getty Images</h1>";
    $admin_url = admin_url('options-general.php?page=wp-getty-images');
    $params = "?response_type=token&client_id=qeuwfr9qkdfmrjbjdub7ph3x&state=datasentfromclient&redirect_uri=";
    $api_url = "https://api.gettyimages.com/oauth2/auth/";
    ?>
    <form method="GET" action="<?php echo $api_url; ?>">
    <input type="hidden" name="response_type" value="token">
    <input type="hidden" name="client_id" value="qeuwfr9qkdfmrjbjdub7ph3x">
    <input type="hidden" name="state" value="datasentfromclient">
    <input type="hidden" name="redirect_uri" value="<?php echo $admin_url; ?>">
    <?php //settings_fields( 'wp-getty-images' );	//pass slug name of page, also referred
                                            //to in Settings API as option group name
    //do_settings_sections( 'wp-getty-images' ); 	//pass slug name of page
    //submit_button();
    ?>
    <button type="submit">Submit</button>
    </form>
    <?php
    
}

 // ------------------------------------------------------------------
 // Add all your sections, fields and settings during admin_init
 // ------------------------------------------------------------------
 //
 add_action( 'admin_init', 'pd_settings_api_init' );
function pd_settings_api_init() {
    // Add the section to reading settings so we can add our
    // fields to it
    add_settings_section(
       'pd_setting_section',
       'Account Credintials',
       'pd_setting_section_callback_function',
       'wp-getty-images'
   );
    
    // Add the field with the names and function to use for our new
    // settings, put it in our new section
   add_settings_field(
        'pd_wp_api_username',
        'Username',
        'pd_setting_input_text',
        'wp-getty-images',
        'pd_setting_section',
        array(
            'label_for' => 'pd_wp_api_username',
            'description' => ''
        )
    );
    add_settings_field(
        'pd_wp_api_password',
        'Password',
        'pd_setting_input_text',
        'wp-getty-images',
        'pd_setting_section',
        array(
            'label_for' => 'pd_wp_api_password',
            'description' => ''
        )
    );
    // add_settings_field(
    //     'pd_wp_api_key',
    //     'API Key',
    //     'pd_setting_input_text',
    //     'wp-getty-images',
    //     'pd_setting_section',
    //     array(
    //         'label_for' => 'pd_wp_api_key',
    //         'description' => 'Please enter your api key. This can be found'
    //     )
    // );  
    // Register our setting so that $_POST handling is done for us and
    // our callback function just has to echo the <input>
    register_setting( 'wp-getty-images', 'pd_wp_api_key' );
    register_setting( 'wp-getty-images', 'pd_wp_api_password', "handle_sanitization_validation_escaping_text" );
    register_setting( 'wp-getty-images', 'pd_wp_api_username' );
} // pd_settings_api_init()

 // ------------------------------------------------------------------
 // Settings section callback function
 // ------------------------------------------------------------------
 //
 // This function is needed if we added a new section. This function 
 // will be run at the start of our section
 //
 
 function pd_setting_section_callback_function() {
    echo '<p>Connect to your Getty Images account to access all content and usage rights available in your subscription.</p>';
}

// ------------------------------------------------------------------
// Callback function for our example setting
// ------------------------------------------------------------------
//
// creates a checkbox true/false option. Other types are surely possible
//

function pd_setting_callback_function() {
    echo '<input name="wp_getty_api_key" id="wp_getty_api_key" type="text" value="" class="code" ' . checked( 1, get_option( 'eg_setting_name' ), false ) . ' />';
    echo '<p>Please enter your api key. This can be found </p>';
}

function pd_setting_input_text($args) {
    echo '<input name="'.$args['label_for'].'" id="'.$args['label_for'].'" type="text" value="'. get_option( $args['label_for'] ) .'" class="code" />';
    echo '<p>'.$args['description'].'</p>';
}

function handle_sanitization_validation_escaping_text($option)
{
    $admin_url = admin_url('options-general.php?page=wp-getty-images');
    $params = "?response_type=token&client_id=qeuwfr9qkdfmrjbjdub7ph3x&state=datasentfromclient&redirect_uri=".$admin_url;
    $api_url = "https://api.gettyimages.com/oauth2/auth/$params";

    header("Location: $api_url");
  
  return $option;
}
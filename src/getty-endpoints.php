<?php

add_action( 'rest_api_init', function () {
	register_rest_route( 'gettyimages/v1', '/search', array(
		'methods' => 'GET',
        'callback' => 'pd_getty_search',
        'permission_callback' => function () {
            return current_user_can( 'edit_others_posts' );
        }
	) );
} );

add_action( 'rest_api_init', function () {
	register_rest_route( 'gettyimages/v1', '/download', array(
		'methods' => 'GET',
        'callback' => 'pd_download_getty_image',
        'permission_callback' => function () {
            return current_user_can( 'edit_others_posts' );
        }
	) );
} );

add_action( 'rest_api_init', function () {
	register_rest_route( 'gettyimages/v1', '/images', array(
		'methods' => 'GET',
        'callback' => 'pd_get_image_details',
        'permission_callback' => function () {
            return current_user_can( 'edit_others_posts' );
        }
	) );
} );

add_action( 'rest_api_init', function () {
	register_rest_route( 'gettyimages/v1', '/media', array(
		'methods' => 'GET',
        'callback' => 'pd_get_media_id',
        'permission_callback' => function () {
            return current_user_can( 'edit_others_posts' );
        }
	) );
} );

function pd_get_media_id()
{
	if(!isset($_GET['id'])){
		return false;
	}

	$post_id = $_GET['id'];

	if($post_id){

		$is_getty_source = get_post_meta( $post_id, 'getty_details', true);
		if($is_getty_source){

			if(isset($_GET['credit_line'])){

				if($is_getty_source['credit_line'] != $_GET['credit_line']){
					$is_getty_source['credit_line'] = $_GET['credit_line'];
					update_post_meta( $post_id, 'getty_details',$is_getty_source);
				}
				
				return $is_getty_source['credit_line'];
			}else{
				if(isset($_GET['check']) && $_GET['check'] == 'caption'){
					
					return (wp_get_attachment_caption( $post_id )) ? wp_get_attachment_caption( $post_id ) : $is_getty_source['img_credit'];
				}

				return ($is_getty_source['credit_line']) ? $is_getty_source['credit_line'] : $is_getty_source['img_credit'];
			}

			
		}else{
			return "Not Getty Image";
		}

	}else{
		return "No ID Found: $post_id";
	}
	
}
function pd_get_image_details()
{
	if(!isset($_GET['imageID'])){
		return false;
	}

	$gettyImageID = intval($_GET['imageID']);
	$url = "https://api.gettyimages.com/v3/images/".$gettyImageID."?fields=detail_set";
	$access_token = get_getty_token();
	$apikey = "2mw9g8hjscucx3hwb4xgsa7r";

	$headers = array( "Api-Key: " . $apikey, "Authorization: Bearer " . $access_token );
	$post_fields = array( "fields" => "detail_set" );	
	$result = pd_make_api_request("images/".$gettyImageID, "GET", $headers, $post_fields);
	$result = ( isset($result['images']) ) ? $result['images'] : "Image not found!";
	return $result;

}

function pd_download_getty_image()
{

	require_once ABSPATH . "wp-admin/includes/media.php";
	require_once ABSPATH . "wp-admin/includes/file.php";
	require_once ABSPATH . "wp-admin/includes/image.php";
	
	$apikey = "2mw9g8hjscucx3hwb4xgsa7r";
	$gettyImageID = $_GET['img_id']; 

	// parameters passed from getty
	$post_id = isset($_GET['post_id']) ? intval( $_GET['post_id'] ) : 0; 
	$img_caption = isset($_GET['img_caption']) ? sanitize_textarea_field($_GET['img_caption']) : ""; 
	$img_alt = isset($_GET['img_alt']) ? sanitize_textarea_field($_GET['img_alt']) : "";
	$img_title = isset($_GET['img_title']) ? sanitize_text_field($_GET['img_title']) : "";
	$img_sizes = isset($_GET['img_sizes']) ? sanitize_text_field($_GET['img_sizes']) : ""; 
	$img_artist = isset($_GET['img_artist']) ? sanitize_text_field($_GET['img_artist']) : ""; 
	$img_credit = isset($_GET['img_credit']) ? sanitize_text_field($_GET['img_credit']) : ""; 
	
	// get access token
	$access_token = get_getty_token();

	/**
	 * * Get Download link from Getty
	 */
	$headers = array( "Api-Key: " . $apikey, "Authorization: Bearer " . $access_token );
	$post_fields = array( "auto_download" => "false", "height" => $img_sizes );
	$result = pd_make_api_request("downloads/images/".$gettyImageID,"POST",$headers,$post_fields);
	$download_url = ( isset($result['uri']) && !empty($result['uri']) ) ? $result['uri'] : false ;
	
	if( !$download_url ) {
		return $result;
	}
	
	$download_url = esc_url_raw( $download_url ); // the image download link from getty
	// error_log(print_r( "getty download link: " . $download_url, true ));

	/**
	 * * Save image to WP Media Gallery
	 */
	$filename = tpd_clean_string($img_title) . '-' . $gettyImageID . '.jpg';
	$uploaddir = wp_upload_dir();
	$uploadfile = $uploaddir['path'] . '/' . $filename;
	$contents = file_get_contents($download_url);
	$savefile = fopen($uploadfile, 'w');
	fwrite($savefile, $contents);
	fclose($savefile);

	$wp_filetype = wp_check_filetype(basename($filename), null );
	// error_log(print_r("wp_filetype",true));
	// error_log(print_r($wp_filetype,true));
	$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title' => $filename,
			'post_content' => '',
			'post_status' => 'inherit'
	);

	$wpImageID = wp_insert_attachment( $attachment, $uploadfile ); // add original image to wp gallery
	// error_log(print_r("attach_id: " . $wpImageID,true));

	if( is_wp_error( $wpImageID ) ) {
		return "Failed to download image to WP Gallery";
	}

	// generate thumbnails and link them to new img
	$fullsizepath = get_attached_file( $wpImageID );
	// error_log(print_r("fullsizepath: " . $fullsizepath,true));
	$attach_data = wp_generate_attachment_metadata( $wpImageID, $fullsizepath );
	wp_update_attachment_metadata( $wpImageID, $attach_data );

	// add getty details to image
	$getty_details = array();
	$getty_details['ID'] = $gettyImageID;
	$getty_details['img_credit'] = $img_credit;
	$getty_details['img_artist'] = $img_artist;
	$getty_details['credit_line'] = "Photo by" . $img_artist . " / " . $img_credit;
	update_post_meta( $wpImageID, 'getty_details', $getty_details );

	// add image alt from getty data
	update_post_meta( $wpImageID, '_wp_attachment_image_alt', $img_alt );

	// prepare obj info to return
	$image_medium_size = wp_get_attachment_image_src( $wpImageID, "medium", false ); // get image url

	$updated = wp_update_post( array(
		'ID' => $wpImageID,
		'post_title' => $img_title,
		'post_content' => $img_caption,
		'post_excerpt' => $img_caption,
		'guid' => $image_medium_size[0],
	) );

	$img_obg = get_post( $updated );
	$img_obg->guid = $image_medium_size[0];

	return $img_obg;

}

/**
 * * Search Image on Getty
 */
function pd_getty_search()
{

	$apikey = "2mw9g8hjscucx3hwb4xgsa7r";
	$search_query = isset( $_GET['s'] ) ? $_GET['s'] : false ;
	$page = isset( $_GET['page'] ) ? $_GET['page'] : 1 ;
	$orientations = isset( $_GET['orientations'] ) ? '&orientations='.$_GET['orientations'] : '&orientations=square,horizontal,panoramic_horizontal' ;
	$sort_order = isset( $_GET['sort_order'] ) ? '&sort_order='.$_GET['sort_order'] : '' ;
	$compositions = isset( $_GET['compositions'] ) ? '&compositions='.$_GET['compositions'] : '' ;
	$editorial_segments = isset( $_GET['editorial_segments'] ) ? '&editorial_segments='.$_GET['editorial_segments'] : '' ;

	if( !$search_query ){
		$error['message'] = "Please enter a search term";
		$error['error'] = true;
		$error['results'] = array();
		return $error;
	}

	$access_token = get_getty_token();

	$images = array();

	$headers = array(
		"Api-Key: " . $apikey,
		"Authorization: Bearer " . $access_token
	);
	$post_fields = array(
		"page" => $page,
		"page_size" => 12,
		"fields" => "id,caption,title,detail_set,download_sizes$orientations$sort_order$compositions",
		"phrase" => urlencode($search_query)
	);
	
	$search_response = pd_make_api_request("search/images/editorial","GET",$headers,$post_fields);
	$images = ( isset($search_response["images"]) && !empty($search_response["images"]) ) ? $search_response["images"] : array() ;
	return $images;
}

/**
 * * Create Access Token
 * * https://github.com/gettyimages/gettyimages-api/blob/master/code-samples/php/image-search.php
 */
function get_getty_token() {

		$apikey = "2mw9g8hjscucx3hwb4xgsa7r";
		$secret = 'TazncxAgxyAmotaud4eT';

		$now = current_time( 'mysql' );
		$current_token = maybe_unserialize( get_option('getty_images_token') );

		if (
			isset($current_token['access_token']) &&
			isset($current_token['expires_in']) &&
			$current_token['access_token'] &&
			strtotime($current_token['expires_in']) > strtotime( $now )
		) {
			return $current_token['access_token'];
		} else {

			$token = get_new_token();
			$later = date( 'Y-m-d H:i:s', strtotime( $now ) + $token['expires_in'] );
			$token['expires_in'] = $later;
			$new_value = maybe_serialize( $token );
			update_option( 'getty_images_token', $new_value );

			// error_log(print_r("access token",true));
			// error_log(print_r($token['access_token'],true));

			return $token['access_token'];

		}

}

function get_new_token() {

	$apikey = "2mw9g8hjscucx3hwb4xgsa7r";
	$secret = 'TazncxAgxyAmotaud4eT';
	$url = "https://api.gettyimages.com/oauth2/token";
	$args = array(
		'method' => 'POST',
		'headers' => array (
				"Cache-Control: no-cache",
				"Content-Type: application/x-www-form-urlencoded",
		),
		'body' => array (
				'grant_type' => 'client_credentials',
				'client_id' => $apikey,
				'client_secret' => $secret
		),
		'httpversion' => '1.1',
		'timeout' => '30',
		'redirection' => '10',
	);
	$curl = new WP_HTTP();
	$result = $curl->request( $url, $args );
	if ( is_wp_error($result) ) {
			$msg = $result->get_error_message();
			return false;
	}
	return $token = json_decode($result['body'], true); // access_token, expires_in, token_type

}

/**
 * * Make API Request
 * * https://github.com/gettyimages/gettyimages-api/blob/master/code-samples/php/image-search.php
 */
function pd_make_api_request($url,$method,$headers = array(),$post_fields = array()) {

	$request_url = "https://api.gettyimages.com/v3/" . $url;

	if(count($post_fields) > 0){
		$request_url .= "?";
		$last_param = end($post_fields);
		foreach ($post_fields as $key => $value) {
			if($last_param == $value){
				$request_url .= $key . "=" . $value;
			}else{
				$request_url .= $key . "=" . $value . "&";
			}
			
		}
		$post_fields = array();
	}
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => $request_url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => $method,
		CURLOPT_HTTPHEADER => $headers,
		CURLOPT_POSTFIELDS => $post_fields
	  ));
	 
		
	$response = curl_exec($curl);
	
	$err = curl_error($curl);
	curl_close($curl);
	if ($err) {
		return "cURL Error #:" . $err;
	}
	return json_decode($response, true);
}

/**
 * * Helper Functions
 */

function tpd_clean_string($string) {
	$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}